//
//  VideoTableViewCell.swift
//  desafioFilmr
//
//  Created by Caio Hikaru Aguena on 13/10/19.
//  Copyright © 2019 Caio Hikaru Aguena. All rights reserved.
//

import UIKit

class VideoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgViewVideoThumb: UIImageView!
    @IBOutlet weak var lblVideoTitle: UILabel!
    
    static let reuseIdentifier: String = String(describing: VideoTableViewCell.self)
    
    static var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
