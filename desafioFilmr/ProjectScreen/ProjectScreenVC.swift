//
//  ViewController.swift
//  desafioFilmr
//
//  Created by Caio Hikaru Aguena on 13/10/19.
//  Copyright © 2019 Caio Hikaru Aguena. All rights reserved.
//

import UIKit

class ProjectScreenVC: UIViewController {
    
    @IBOutlet weak var tableViewVideoList: UITableView!
    
    // URL do JSON
    let apiURL = "https://elasticbeanstalk-us-east-1-508049151276.s3.amazonaws.com/FilmrTest/db.json";
      
    var jsonData = apiData()
    let imageCache = NSCache<NSString, UIImage>()
    
    // Parse dos dados da API
    struct apiData: Codable {
        
        struct filmr_project: Codable {
            let name: String
            let thumbnail_url: URL
            let video_url: URL
        }
        
        var filmr_site: String
        var filmr_projects: [filmr_project]
        var filmr_logo: String
        
        init() {
            filmr_site = ""
            filmr_projects = []
            filmr_logo = ""
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Project Screen"
        tableViewVideoList.dataSource = self
        tableViewVideoList.delegate = self
        
        self.tableViewVideoList.register(VideoTableViewCell.nib, forCellReuseIdentifier: VideoTableViewCell.reuseIdentifier)
        loadJSON()
    }
    
    // Carrega o JSON com os dados da API
    func loadJSON() {
        guard let url = URL(string: apiURL) else {return}
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let dataResponse = data,
                error == nil else {
                    print(error?.localizedDescription ?? "Response Error")
                    return }
            do {
                let decoder = JSONDecoder()
                self.jsonData = try decoder.decode(apiData.self, from: dataResponse)
                DispatchQueue.main.async { self.tableViewVideoList.reloadData() }
            } catch let parsingError {
                print("Error", parsingError)
            }
        }
        task.resume()
    }
    
}

extension ProjectScreenVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: VideoTableViewCell.reuseIdentifier)! as! VideoTableViewCell

        // Verifica se há imagem no cache, se não carrega uma nova imagem e guarda no cache
         if let cachedImage = self.imageCache.object(forKey: NSString(string: self.jsonData.filmr_projects[indexPath.row].name)) {
             cell.imgViewVideoThumb.image = cachedImage
         } else {
             DispatchQueue.global(qos: .background).async {
                 let url = self.jsonData.filmr_projects[indexPath.row].thumbnail_url
                 let data = try? Data(contentsOf: url)
                 let image: UIImage = UIImage(data: data!)!
                 DispatchQueue.main.async {
                     self.imageCache.setObject(image, forKey: NSString(string: self.jsonData.filmr_projects[indexPath.row].name))
                     cell.imgViewVideoThumb.image = image
                 }
             }
         }
         
         cell.lblVideoTitle.text = jsonData.filmr_projects[indexPath.row].name
         return cell
     }
    
}

extension ProjectScreenVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return jsonData.filmr_projects.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableViewVideoList.deselectRow(at: indexPath, animated: true)
        
        let newViewController = VideoPlayerVC(nibName: "VideoPlayerVC", bundle: nil)
        
        newViewController.videoURL = self.jsonData.filmr_projects[indexPath.row].video_url
        newViewController.videoTitle = self.jsonData.filmr_projects[indexPath.row].name
        
        navigationController?.pushViewController(newViewController, animated: true)
    }
    
}
