//
//  VideoPlayerVC.swift
//  desafioFilmr
//
//  Created by Caio Hikaru Aguena on 15/10/19.
//  Copyright © 2019 Caio Hikaru Aguena. All rights reserved.
//

import UIKit
import AVFoundation

class VideoPlayerVC: UIViewController {
    
    @IBOutlet weak var videoLayerView: UIView!
    @IBOutlet weak var playPauseButton: UIButton!
    @IBOutlet weak var videoSlider: UISlider!
    @IBOutlet weak var videoDurationLabel: UILabel!
    @IBOutlet weak var downloadButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    
    var player: AVPlayer?
    var videoURL: URL?
    var videoTitle: String?
    var timeObserver: Any?
    var timer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.landscape, andRotateTo: UIInterfaceOrientation.landscapeLeft)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupVideoPlayer()
        resetTimer()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(toggleControls))
        view.addGestureRecognizer(tapGesture)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.all)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override var prefersHomeIndicatorAutoHidden: Bool {
        return true
    }
    
    func resetTimer() {
        timer?.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 3.5, target: self, selector: #selector(hideControls), userInfo: nil, repeats: false)
    }
    
    @objc func hideControls() {
        playPauseButton.isHidden = true
        videoSlider.isHidden = true
        videoDurationLabel.isHidden = true
        downloadButton.isHidden = true
        backButton.isHidden = true
    }
    
    @objc func toggleControls() {
        playPauseButton.isHidden = !playPauseButton.isHidden
        videoSlider.isHidden = !videoSlider.isHidden
        videoDurationLabel.isHidden = !videoDurationLabel.isHidden
        downloadButton.isHidden = !downloadButton.isHidden
        backButton.isHidden = !backButton.isHidden
        resetTimer()
    }
    
    func setupVideoPlayer() {
        guard let url = videoURL else {
            return
        }
        player = AVPlayer(url: url)
        
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = videoLayerView.bounds;
        playerLayer.videoGravity = .resizeAspect
        playerLayer.needsDisplayOnBoundsChange = true
        videoLayerView.layer.addSublayer(playerLayer)
        player?.play()
        
        let interval = CMTime(seconds: 0.01, preferredTimescale: CMTimeScale(NSEC_PER_SEC))
        timeObserver = player?.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main, using: { elapsedTime in
            self.updateVideoPlayerSlider()
        })
    }
    
    @IBAction func playPauseTap(_ sender: Any) {
        guard let player = player else { return }
        if !player.isPlaying {
            player.play()
            playPauseButton.setImage(UIImage(named: "pause"), for: .normal)
        } else {
            playPauseButton.setImage(UIImage(named: "play"), for: .normal)
            player.pause()
        }
    }
    
    func updateVideoPlayerSlider() {
        guard let currentTime = player?.currentTime() else { return }
        let currentTimeInSeconds = CMTimeGetSeconds(currentTime)
        videoSlider.value = Float(currentTimeInSeconds)
        if let currentItem = player?.currentItem {
            let duration = currentItem.duration
            if (CMTIME_IS_INVALID(duration)) {
                return;
            }
            let currentTime = currentItem.currentTime()
            videoSlider.value = Float(CMTimeGetSeconds(currentTime) / CMTimeGetSeconds(duration))
            
            let mins = currentTimeInSeconds / 60
            let secs = currentTimeInSeconds.truncatingRemainder(dividingBy: 60)
            let timeformatter = NumberFormatter()
            timeformatter.minimumIntegerDigits = 2
            timeformatter.minimumFractionDigits = 0
            timeformatter.roundingMode = .down
            guard let minsStr = timeformatter.string(from: NSNumber(value: mins)), let secsStr = timeformatter.string(from: NSNumber(value: secs)) else {
                return
            }
            videoDurationLabel.text = "\(minsStr):\(secsStr)"
        }
    }
    
    @IBAction func sliderValueChanged(_ sender: Any) {
        guard let duration = player?.currentItem?.duration else { return }
        let value = Float64(videoSlider.value) * CMTimeGetSeconds(duration)
        let seekTime = CMTime(value: CMTimeValue(value), timescale: 1)
        player?.seek(to: seekTime )
    }
    
    @IBAction func downloadTap(_ sender: Any) {
        DownlondFromUrl(fileURL: videoURL!)
    }
    
    @IBAction func returnTap(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func DownlondFromUrl(fileURL: URL){
        
        // Inicia animação de feedback quando está baixando
        UIView.animate(withDuration: 0.3, delay: 0.5, options: [.curveEaseOut, .repeat, .autoreverse], animations: {
            self.downloadButton.alpha = 0.0
        }, completion: nil)
        
        // Create destination URL
        let documentsUrl:URL =  (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL?)!
        let destinationFileUrl = documentsUrl.appendingPathComponent(videoTitle!)
        
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)
        
        let request = URLRequest(url:fileURL)
        
        // Tenta baixar o video que está tocando noplayer atualmente
        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
            if let tempLocalUrl = tempLocalUrl, error == nil {
                
                // Success
                if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                    
                    DispatchQueue.main.async {
                        self.downloadButton.layer.removeAllAnimations()
                        self.downloadButton.alpha = 1
                        self.showAlert(Mensagem: "Download feito com sucesso.]")
                    }
                    NSLog("Download feito com sucesso \(statusCode)")
                }
                
                do {
                    try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                } catch (let writeError) {
                    self.showAlert(Mensagem: "Arquivo ja existe")
                    NSLog("Arquivo ja existe: \(destinationFileUrl) : \(writeError)")
                }
                
            } else {
                self.showAlert(Mensagem: "Erro ao salvar o arquivo")
            }
        }
        
        task.resume()
    }
    
    // Mostra uma mensagem para o usuário
    func showAlert(Mensagem: String) {
        let alert = UIAlertController(title: Mensagem, message: "", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
}

extension AVPlayer {
    var isPlaying: Bool {
        return rate != 0 && error == nil
    }
}

